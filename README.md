```bash
Name        : Khalis Murfid :)
NPM         : 1706040164
Class       : PPW-C
Herokuapp   : djangokhalis.herokuapp.com/konidin
```

## Application Status
[![pipeline status](https://gitlab.com/khalismurfid/django-project-ppw/badges/master/pipeline.svg)](https://gitlab.com/khalismurfid/django-project-ppw/commits/master)
[![coverage report](https://gitlab.com/khalismurfid/django-project-ppw/badges/master/coverage.svg)](https://gitlab.com/khalismurfid/django-project-ppw/commits/master)
from django.urls import path

from home import views

urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home, name='home'),
    path('resume/', views.resume, name='resume'),
    path('portofolio/', views.portofolio, name='portofolio'),
    path('guestbook/', views.guestbook, name='guestbook'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/add/', views.schedule_add, name='schedule_add'),
    path('delete_all', views.delete_all, name='delete_all'),

]
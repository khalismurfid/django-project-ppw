# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from .models import Schedule
from . import forms




# Create your views here.

def home(request):
	return render(request, "home.html")


def resume(request):
	return render(request, "Resume.html")


def portofolio(request):
	return render(request, "portofolio.html")


def guestbook(request):
	return render(request, "guestbook.html")

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})


def schedule_add(request):
    if request.method == 'POST':
        form = forms.PostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.PostForm()
    return render(request, 'schedule_add.html', {'form': form})

def delete_all(request):
    schedules=Schedule.objects.all().delete()
    return render(request, 'schedule.html', {'schedules': schedules})

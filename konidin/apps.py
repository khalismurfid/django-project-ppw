from django.apps import AppConfig


class KonidinConfig(AppConfig):
    name = 'konidin'

$(function(){
    $("#accordion").accordion(
    {   collapsible: true, 
        active: false
    });
} );

$(function(){
    var darkMode = localStorage.getItem('darkMode') || true;
    $("#change").click(

        function(){
            $("body").fadeIn("3000");
            if(darkMode){
                $(this).css({"color" : "rgb(25,25,25)"});
                $(this).css({"background-color" : "rgba(255,255,255,.75)"});
                $(this).text("Dark Mode")
                $("body").css({"backgroundColor" : "rgba(255,255,255,.75)"});
                $("body").css({"color" : "rgb(25,25,25)"});
                localStorage.setItem('darkMode',true);
                darkMode=false;
            }
            else{
                $(this).css({"color" : "rgba(255,255,255,.75)"});
                $(this).css({"background-color" : "rgb(25,25,25)"});
                $(this).text("Light Mode")
                $("body").css({"backgroundColor" : "rgb(25,25,25)"});
                $("body").css({"color" : "rgba(255,255,255,.75)"});
               localStorage.setItem('darkMode',false);
                darkMode=true;
            }
    });
});

$(document).ready(function() {
    var counter = 0;

  // Start the changing images
  setInterval(function() {
    if(counter == 10) {
      counter = 0;
    }

    changeImage(counter);
    counter++;
  }, 1500);
  loading();
});

function changeImage(counter) {
  var images = [
    'Sabar',
    'Bentar',
    'Mantap',
    'Belum',
    'Melengkung',
    'Jaket',
    'Kuning',
    'Siap',
      'Menikung'
  ];

  $(".loader .image").html(""+ images[counter] +"");
}

function loading(){
  var num = 0;
  for(i=0; i<=100; i++) {
    setTimeout(function() {
      $('.loader span').html(num+'%');

      if(num == 100) {
          $('.loader').hide();
          window.setTimeout(function(){
                $(".page").css({"visibility" : "visible"});
                  }, 1000);


      }
      num++;
    },i*50);
  };

}




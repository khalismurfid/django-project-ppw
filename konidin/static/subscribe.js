function subscribe(){

    $.ajax({
        url: "/konidin/subscribe/",
        type: "POST",
        data:{
            full_name: $('#name_form').val(),
            email: $('#email_form').val(),
            password: $('#password_form').val(),
            csrfmiddlewaretoken : document.getElementsByName('csrfmiddlewaretoken')[0].value
        },

        success: function(result){
            if(result["message"]=="berhasil"){
                $('#name_form').val("");
                $('#email_form').val("");
                $('#password_form').val("");
                $('#subscriber').html("");
                getSubscriber();
                alert("Subscribed");
            }
            else{
                alert("Something is wrong");
            }
            
           
        },

        error: function(xhr, errmsg, err){
            alert(errmsg);
        },
    });
};

function validate(){
    $.ajax({
        url: "/konidin/api/validateEmail/",
        type: "POST",
        data: {
            full_name: $('#name_form').val(),
            email: $('#email_form').val(),
            password: $('#password_form').val(),
            csrfmiddlewaretoken : document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        success: function(result) {
            $('#notes').html(result['message'])
            if(result['message']== "belum terdaftar"){
                document.getElementById('subscribe_button').disabled = false;
            }
            else{
                document.getElementById('subscribe_button').disabled = true;
            }
        },
        error : function (errmsg){
            alert("Something is wrong");
        }
    });
};

function deleteSubscriber(id){
    $.ajax({
        url: "/konidin/api/delSubscriber/",
        type: "POST",
        data: {
            pk: id,
            csrfmiddlewaretoken : document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        success: function(result) {
            console.log($(this));
             $('#subscriber').html("");
            getSubscriber();
           alert("Unsubscribed");
        },
        error : function (errmsg){
            alert("Something is wrong");
        }
    });
};

function getSubscriber(){
     $.ajax({
        url: "/konidin/api/getSubscriber/",
        type: "GET",
        success: function(result){
            console.log(result[0]);
            for(i=0; i< result.length;i++){
                $('#subscriber').append("<div class='row'style='padding-bottom:10px;'><div class='col-sm-6'>"+result[i]['fields']['email']+"</div><div class='col-sm-6'><button onclick='deleteSubscriber("+
                                        result[i]['pk']+
                                        ")' class='btn btn-danger'"+">Unsubscribe</button></div></div>");
            }
            
        },
        error: function(errmsg){
            alert("errmsg");
        }
    });
};

$(document).ready(function(){
   getSubscriber();
});

$(document).ready(function () {
    var x_timer;
    $("#email_form").keyup(function (e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {
            validate();
        }, 10);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#name_form").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {
            validate();
        }, 10);

    });
});

$(document).ready(function () {
    var x_timer;
    $("#password_form").keyup(function (e) {
        clearTimeout(x_timer);
        var nama = $(this).val();
        x_timer = setTimeout(function () {
            validate();
        }, 10);
    });
});



from django.shortcuts import render,redirect
from .models import Status,Subscribe
from .forms import PostForm, SubscribeForm
from django.http import JsonResponse
from django.core import serializers
from django.http import HttpResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
import urllib.request, json
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
import requests

response={}

# Create your views here.
def status(request):
    status = Status.objects.all().order_by('date')[::-1]
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('landingpage')
    else:
        form = PostForm()

    return render(request, 'landingPage.html',{'schedules': status,'form': form})

def profile(request):
    return render(request, 'profile.html')

def subscribe(request):
    if request.method == 'POST':
        try:
            print(request.POST['email'])
            validate_email(request.POST['email'])
        except:
            response["message"] = "Wrong email format"
            return JsonResponse(response)
        form = SubscribeForm(request.POST)
        if(form.is_valid()):
            form.save()
        response["message"] = "berhasil"
        return JsonResponse(response)
    else:
        form = SubscribeForm()
        return render(request, 'subscribe.html',{"forms":form})

def get_subscriber(request):
    if request.method == 'GET':
        lst_subscriber = serializers.serialize('json', Subscribe.objects.all())
        response = lst_subscriber

    else:
        response['message'] = 'Method POST not ALLOWED HERE'
    return HttpResponse(response, content_type='application/json')
def del_subscriber(request):
    if request.method == 'POST':
        subscriber = Subscribe.objects.get(pk=request.POST.get('pk'))
        subscriber.delete()
        response['message'] = "success"
    else:
        response['message'] = "Method GET not allowed here"
    return JsonResponse(response)


def checkmail(request):
    if request.method == 'POST':
        try:
            validate_email(request.POST['email'])
        except:
            response["message"] = "Wrong email format"
            return JsonResponse(response)
        if(request.POST.get('full_name')=="" or request.POST.get('password') ==""):
            response['message'] = "Please fill the blank"
            return JsonResponse(response)

        if(request.POST.get('password')is not None ):
            if(len(request.POST.get('password'))<8 ):
                response['message'] = "Password must be greater than 7"
                return JsonResponse(response)
        isRegistered = Subscribe.objects.filter(email=request.POST['email'])
        if(isRegistered):
            response["message"] = "sudah terdaftar"
        else:
            response["message"] = 'belum terdaftar'
        return JsonResponse(response)
    else:
        response["message"] = "GET METHOD NOT ALLOWED HERE"
        return JsonResponse(response)

def redirectStatus(request):
    return redirect('landingpage')

def liblary(request, search ='quilting'):
    
    if  request.user.is_authenticated :
        if "like" not in request.session:
            request.session["fullname"]=request.user.first_name+" "+request.user.last_name
            request.session["username"]=request.user.username
            request.session["email"] = request.user.email
            request.session["sessionid"]=request.session.session_key
            request.session["like"]=[]
            
        return render(request, 'liblary.html', {"search":search})
    else:
        return redirect('login') 
               
   
@csrf_exempt
def like(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] not in lst :
            lst.append(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)    
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")
    

@csrf_exempt
def unlike(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] in lst :
            lst.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)    
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")
    
def get_like(request):
    if request.user.is_authenticated :
        if(request.method=="GET"):
            if request.session["like"] is not None:
                response["message"] = request.session["like"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)

def book_data(request, search):
    raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q='+search).json()
    books =[]
    for info in raw_data['items']:
        data = {}
        if 'imageLinks' in info['volumeInfo']:
            data['cover'] = info['volumeInfo']['imageLinks']['thumbnail']
        else:
            data['cover'] = "http://www.scottishbooktrust.com/files/styles/book-cover-book-page/public/cover-not-available_201.png?itok=XV_bm-Xa"
        if 'title' in info['volumeInfo']:
            data['title'] = info['volumeInfo']['title']
        else:
            data['title'] = "Tidak ada"
        if 'authors' in info['volumeInfo']:
            data['authors'] = ", ".join(info['volumeInfo']['authors'])
        else:
            data['authors'] = "Tidak ada"

        if 'publishedDate' in info['volumeInfo']:
            data['publishedDate'] = info['volumeInfo']['publishedDate']
        else:
            data['publishedDate'] = "Tidak ada"
        data['id']=info['id']
        books.append(data)
    return JsonResponse({"data": books})

def loginPage(request):
    return render(request,'login.html')
def logoutPage(request):
    logout(request)
    return redirect('login')

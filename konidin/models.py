# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from datetime import datetime


class Status(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.status

class Subscribe(models.Model):
    full_name = models.CharField(max_length=320)
    email = models.CharField(max_length=300, unique=True)
    password = models.CharField(max_length=300)

    def __str__(self):
        return self.email



# Create your models here.

from django import forms

from .models import Status,Subscribe
from template_forms import bs3

class PostForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        widgets = {
            'status': forms.TextInput(attrs={'placeholder': "What's Happening"}),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
            
class SubscribeForm(bs3.BlockForm, forms.ModelForm):
    class Meta:
        model = Subscribe
        fields = ['full_name','email','password']
        widgets = {
            'full_name' : forms.TextInput(attrs={'placeholder': "Full Name",'id': 'name_form'}),
            'email' : forms.TextInput(attrs={'placeholder': "Email Address", 'id': 'email_form'}),
            'password' :forms.PasswordInput(attrs={'id': 'password_form'})
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
from selenium import webdriver
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import status,profile, subscribe, get_subscriber, book_data
from .models import Status,Subscribe
from .forms import PostForm, SubscribeForm
from django.db import IntegrityError
from selenium.webdriver.chrome.options import Options
import time

client = Client()
# Create your tests here.
class Story6UnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/konidin/landing-page/')
        self.assertEqual(response.status_code, 200)

    def test__using_status_func(self):
        found = resolve('/konidin/landing-page/')
        self.assertEqual(found.func, status)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = status(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar ?', html_response)

    def test_story6_using_to_do_list_template(self):
        response = Client().get('/konidin/landing-page/')
        self.assertTemplateUsed(response, 'landingPage.html')

    def test_model_can_create_status(self):
        new_status = Status.objects.create(status='Aku lagi stres ni kak pew')
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)


    def test_can_post_(self):
        response = self.client.post('/konidin/landing-page/',data={'status':'Aku lagi stres ni kak pew'})
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

        self.assertEqual(response.status_code,302)
        self.assertEqual(response['location'], '/konidin/landing-page/')

        new_response = self.client.get('/konidin/landing-page/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Aku lagi stres ni kak pew', html_response)

    def test_form_status_input_has_placeholder(self):
        form = PostForm()
        self.assertIn('placeholder="What&#39;s Happening"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = PostForm(data={'status': '','date':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/konidin/landing-page/', {'status': test,})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/konidin/landing-page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/konidin/landing-page/', {'status': '','date':''})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/konidin/landing-page/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test__using_profile_func(self):
        found = resolve('/konidin/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_using_to_do_list_template(self):
        response = Client().get('/konidin/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_page_is_completed(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Khalis Murfid', html_response)
        self.assertIn('1706040164', html_response)
        self.assertIn("I'm a good boy", html_response)
        self.assertIn('https://serving.photos.photobox.com/22128934c433d3fcdbe4d1c2c6f59eb94d02949cfd2502d5d798384ebd7f833c5dba102a.jpg" alt="Avatar" class="img-circle"', html_response)

    def test_form_validation_for_blank_items(self):
        form = SubscribeForm(data={'full_name': '','password':'', 'email':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['full_name'],
            ["This field is required."]
        )
    def test_subscribed_user_created(self):
        counter_before_inc = Subscribe.objects.count()
        liked_book = Subscribe.objects.create(email="test@email.com")
        self.assertEqual(counter_before_inc + 1, Subscribe.objects.count())

    def test_unique_email(self):
        Subscribe.objects.create(email="test@email.com")
        with self.assertRaises(IntegrityError):
            Subscribe.objects.create(email="test@email.com")
            
    def test_subscribe_view_post_return_200(self):
        response = client.post('/konidin/subscribe/', data={
                                                "name" : "name",
                                                "email" : "hello@gmail.com",
                                                "password" : "123456",
                                            })
        self.assertEqual(response.status_code, 200)
        
    def test_subscribe_view_get_return_403(self):

        response = client.get('/konidin/subscribe/')
        self.assertEqual(response.status_code, 200)
        
        
    def test_subscribe_views_using_correct_function(self):
        found = resolve('/konidin/subscribe/')
        self.assertEqual(found.func, subscribe)
        
    def test_get_subscribers_api_view_get_return_403(self):

        response = client.get('/konidin/api/getSubscriber/')
        self.assertEqual(response.status_code, 200)
        
        
    def test_get_subscribers_api_views_using_correct_function(self):
        found = resolve('/konidin/api/getSubscriber/')
        self.assertEqual(found.func, get_subscriber)
        
    def test_search_books_api_view_get_return_403(self):

        response = client.get('/konidin/liblary/api/books/quilting/')
        self.assertEqual(response.status_code, 200)
        
        
    def test_search_books_api_views_using_correct_function(self):
        found = resolve('/konidin/liblary/api/books/quilting/')
        self.assertEqual(found.func, book_data)
        
        




class Story6FunctionalTests(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')    
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTests, self).setUp()
    def tearDown(self):
        time.sleep(5)
        self.browser.quit()
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://djangokhalis.herokuapp.com/konidin/landing-page/')
        element = self.browser.find_element_by_id("id_status");
        element.send_keys("Coba Coba")
        element.submit()
        assert "Coba Coba" in self.browser.page_source
    
    def test_css(self):
        self.browser.get('http://djangokhalis.herokuapp.com/konidin/landing-page/')
        content = self.browser.find_element_by_tag_name("h4").value_of_css_property('color')
        self.assertIn('rgba(255, 255, 255, 0.75)', content)
        time.sleep(1)
    def test_title(self):
        self.browser.get('http://djangokhalis.herokuapp.com/konidin/landing-page/')
        self.assertIn('Khalis CV', self.browser.title)
        time.sleep(1)
    
#    def test_changedarLightkMode(self):
#        self.browser.get('http://djangokhalis.herokuapp.com/konidin/')
#        changeLight = self.browser.find_element_by_link_text("Light Mode")
#        changeLight.click()
#        content = self.browser.find_element_by_id("ui-id-1").value_of_css_property('color')
#        self.assertIn('rgb(25, 25, 25)', content)
#        changeDark = self.browser.find_element_by_link_text("Dark Mode")
#        changeDark.click()
#        content2 = self.browser.find_element_by_tag_name("ui-id-1").value_of_css_property('color')
#        self.assertIn('rgba(255, 255, 255, .75)', content2)
        
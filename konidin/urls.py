from django.conf.urls import url
from konidin.views import status, profile,loginPage,get_like,unlike,like,redirectStatus,logoutPage, liblary, book_data, subscribe, checkmail, del_subscriber, get_subscriber
from django.urls import path, include, re_path

urlpatterns = [
    url('landing-page/', status, name='landingpage'),
    url('profile/', profile, name='profile'),
    path('liblary/api/books/<str:search>/', book_data, name='books'),
    path('liblary/<str:search>/', liblary),
    path('liblary/', liblary, name='liblary'),
    path('subscribe/', subscribe, name='subscribe'),
    path('api/validateEmail/', checkmail, name='checkmail' ),
    path('api/getSubscriber/', get_subscriber),
    path('api/delSubscriber/', del_subscriber),
    path('api/like/', like),
    path('api/unlike/', unlike),
    path('api/getLike/', get_like),
    path('login/',loginPage,name='login'),
    path('logout/',logoutPage,name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    path('', profile)

]
